author: James Tilton
---

# Intro to Podman

## James Tilton

### Cloud Engineer and Sysmte Administrator

### jamestilton1@gmail.com

### gitlab.com/jamestilton1/podman-presentation

---
## Who Am I

I am system administrator who has supporting servers, network, and all sorts fo technology for 20 years.

I am an open source and technology enthusiast and I am always working on some sort of tech project.

Currently I am working as a Oracle Cloud Engineer for a healthcare company. 

---
## Pole of the Room

- Who has heard about containers?
  - Docker/Podman
- Virtual machines?
- Micro Services?

---

## What we are going to learn about

In this talk we are talk about using podman to interact with containers. 

We will work with a couple containers as well as create our own container image and deploy that container. 

---

## What are containers 

Containers are groups of resources on a Linux system that are isolated from each other.

This is done by using namespaces, a Linux kernel feature, which allows processes to be limited to only other resources wihin that namespace. 

We will specificly be talking about OCI(Open Container Initiative) containers, which is a standard way of describing containers and container images.

The other aspect of containers is that there is an imputable base image that is used to run the container. 

---

## Containers contd

What are the benifits of using containers

- Allows to create reproducible services and applications reguardless of host
    - prevents the it works on my system but not your system
- Allows packaging of application in a light weight manner
    - only the tools, libraries, and files absolutley needed to run the application
- Does not add libraries or applications to host
    - Will not cause library dependency conflits on host
- Multiple instances of similar applications running at the same time. 
    - Can create multiple web servers running in a cluster and have a single load balancer
    - Ports on host will still be limited to outside world

---

## Container System Options

There are two main ways to run containers. Orchestrated and an individual host

Orchestrated options:
- Kubernetes
- Docker Swarm
- Openshift

Individual host:
- Docker
- podman
- lxd

---

## Podman

> Podman is a daemonless, open source, Linux native tool designed to make it easy to find, run, build, share and deploy applications using Open Containers Initiative (OCI) Containers and Container Images. 
> https://docs.podman.io/en/latest/

Podman is a tool that is built to be a 1:1+ replacement of Docker. All Docker commands will work with Podman, excluding Docker Swarm.

What I mean by 1:1+ is that there are some features that Podman has that Docker will not but if a new feature is added to Docker it will be added to Podman. 

Some examples of this are the systemd integration that Podman has, along wtih the Podman service, when ran, is a short live processes that uses systemd to listen for a socket call to launch the service. 

Docker uses a running daemon that manages the containers. 

---

## Podman Cont'd

Why Podman instead of Docker

Podman was created after Docker so it was able to take some the lessons learned to come up with other ways to accomplish the same tasks. 

- Podmans default configuration runs in a rootless manner where the default configuration for Docker runs as a rootful daemon. 
- Running in a rootless configuration can lead to better security practices. Configurations have to be opened to allow the higher privilege
- Docker has a hard coded default repository for images of docker.io, where Podman has the configuration of no repositories configured as default. A bit more verbose but vendor neutral, nonopinionated. Can still use docker.io images
- Docker runs with a daemon that is running as root by default. Where Podmand does not have a daemon running. There is a lightweight service that can run. 

---

## Basics of Podman

Launching your first container:

`podman run -ti --rm registry.access.redhat.com/ubi8/httpd-24 bash`

- run is the command type sent to Podman. There are a lot of different commands 
- -t is to create a tty session
- -i is to tell to run in interactive mode
- --rm states to remove the container when the container is stopped
- registry.access.redhat.com is the repository where the images are stored(docker.io/library is another example)
- ubi8/httpd-24 is the path to the image on the repository 
- bash is to run a command within the container

### Images vs container

Image is a static instance that is a blue print for how a container is brought up.
It has all the files required to launch a container. This image can be used on multiple different containers. 

---

## What is the output

What did the command do.
First it pulled down the image from the repository 
Second podman goes through and grabs the different layers of the container
Finally the command is run, presenting a bash prompt since the command ran in interactive mode. 

At this bash prompt any commnd in the image can be run within the container. Since this is the Redhat Universal Base Image more applications can be installed with dnf. 

Validate that this prompt is a completely different OS

`grep PRETTY_NAME /etc/os-release`

---

## Running more containers

This command will run a container in a more standard method

`podman run -d -p 8080:8080 --name myapp registry.access.redhat.com/ubi8/httpd-24`
- -d run in detached mode
- -p connect host port to container port

With this command there is no final command so the container uses the default command for the container. This information is part of the documentation of the container. 

At this point a browser will be able to connect to the host IP address on port 8080 and connect to the webserver within the contaner. 

---

## Manage Containers

Now that this container is running to monitor it activity the command to use:
`podman ps`

This will output all the running containers.

A stopped container will not display in this output
`podman stop myapp`

This will stop the container called myapp.

To view all running and stopped containers run
`podman ps --all`

---

## Starting a stopped container

The run command is only used to create new containers. To start a stopped container it is the command `start` and the name of the container. 

`podman start myapp`

This will launch the container will all the settings that were previously passed to the container. 

---

## Connecting into running container

Using the exec command to update the html file

`podman exec -i myapp bash -c 'echo "<html><h1>My App</h1></html>" > /var/www/html/index.html'`

This will connect to the bash prompt of the container allow access to all  the files and applications within the container.

`podman exec -it myapp bash`

---

## Custom Images and Layers

Building our own custom image allows for customizations to the image that make the setup the same each time it launches. Changes to the image are not lost each time the image is removed and added. 


To start an image it requires a Containerfile or Docker file in a directory along with all files that are in support of the image. 

Create a directory and create a basic html file

`echo <html><h1>Hellow World</h1></html> > index.html`

Then create a file nammed Containerfile and add the following to the file. 

```Containerfile
FROM registry.access.redhat.com/ubi8/httpd-24

COPY ./index.html /var/www/html/index.html
```

With these two files in a directory run the following command

`podman build -t myapp ./`

```
output:
STEP 1/2: FROM registry.access.redhat.com/ubi8/httpd-24    
STEP 2/2: COPY index.html /var/www/html/index.html
```

```
podman run -d --rm -p 8080:8080 --name webapp myapp
```

This show the hello world as the default index page when the container is brought up. 

This works but is an easier way to manipulate files within a container by using volumes.

---

## Volumes and shared data

Volumes allow for data to be pulled in from the host system. These could be configuration files, data files, databases, or anything else that needs to be passed into the container. 

Volumes are added to containers by default in a read/write configuraiton but can be passed in as read only. 

Here is how to pass in a specific file to replace the index file

```
podman run -d -p 8080:8080 --name webapp -v ./indext.html:/var/www/html/index.html:ro myapp 
```

With this we can see that the index file is now not the hello world that we had before. 

Volumes work on a layering aspect in which it mounts the files overtop the container files resulting in the volume being the one displayed. 

This also makes working with the files easier as there is no need to exec into the container to manipulate the file. 

---

## Pods

Pods which is where Podman got its name are a concept brought over from Kubernetes. This tool allows for a group of containers to use the same namespace and resources. 

This is a functionality that Docker does not have. But this is a way to get into Kubernetes to scale the application. 

To create a pod:

```
podman pod create -p 8080:8080 --name mypod --volume ./html:/var/www/html
```

This command will create the pod called mypod and then allow containers in the pod to share the volume and ports avaiable.

Adding a container to this pod will automatically give the resources meaning they do not need to be specified in the command

```
podman create --pod mypod --name myapp myimage
```

This has just created the pod and attached the container to it. To bring everything up another command needs to run

```
podman pod start mypod
```

This will now launch all containers associated with the pod

---

## Compose

Docker-compose is a tool for creating a group of containers using a yaml file. Orignially a seperate tool completely written in python to work with the docker apis. 

This has been brought into the main docker applicaiton and subsiquently Podman as of version 4.7.

The advantage of having this tooling is there are a lot of multi-service applications that have these compose files avaialble for them making it easy to bring up a service. 

There is an extra configuration that Podman needs to be able to support compse requests. The service needs to be listening to take the requests. This is done with a systemd socket. This service is listening for requests to the podman socket and when it recieves a request it spins up the podman service. After podman finishes the request it waits 5 seconds and spins down the service. 

With this avaialble podman compose can be used to bring up a compose file. 

Another nicety of a compose file is everything about the configuraiton is layed out in the file. 

Example docker-compose.yaml file:
```
version: "3.7"
services:
  myapp:
    image: quay.io/rhatdan/myimage:latest
    volumes:
      - ./html:/var/www/html
      - myapp_vol:/vol
    ports:
      - 8080:80
volumes:
  myapp_vol: {}
```

---

## Conclusion

Overall wtih Podman being a more secure out of the box drop in replacement for Docker it is hard to recommend Docker. With Kubernetes being the more deploy cloud container orchestration tool it makes sense to use Podman.

The knowledge gained building out local systems and images using containers locally allow for more advanced functionality to go into large cloud deployments. 

The tooling that was created for Docker can be used directly with Podman. with the extra features that Podman has it makes itself a compelling case. 

---

### References

- https://docs.podman.io/en/latest/
- Podman in Action by Daniel Walsh
- Podman for Devops by Alessandro Arrichiello, Gianni Salientti



